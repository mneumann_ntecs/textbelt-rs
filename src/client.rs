use crate::PhoneNumber;
use anyhow::{bail, Result};
use serde::{Deserialize, Serialize};
use std::convert::Into;
use thiserror::Error;

pub struct Client {
    api_key: String,
}

const TEXTBELT_URI: &str = "https://textbelt.com/text";

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct TextbeltResponse {
    success: bool,
    quota_remaining: Option<usize>,
    text_id: Option<String>,
    error: Option<String>,
}

#[derive(Serialize, Debug)]
struct TextbeltRequest<'a> {
    phone: &'a str,
    message: &'a str,
    key: &'a str,
}

#[derive(Error, Debug)]
pub enum TextbeltError {
    #[error("Failed")]
    FailedWith(String),

    #[error("Out of quota")]
    OutOfQuota(String),

    #[error("Invalid response")]
    InvalidResponse(Option<String>),
}

#[derive(Debug)]
pub struct Success {
    pub text_id: String,
    pub quota_remaining: usize,
}

impl Client {
    pub fn new(api_key: impl Into<String>) -> Client {
        Client {
            api_key: api_key.into(),
        }
    }

    pub async fn send(&self, phone: &PhoneNumber, message: &str) -> Result<Success> {
        let data = TextbeltRequest {
            phone: &phone.0,
            message,
            key: &self.api_key,
        };

        match surf::post(TEXTBELT_URI).body_json(&data)?.await {
            Ok(mut res) => match res.body_json().await {
                Ok(json) => match json {
                    TextbeltResponse {
                        success: true,
                        text_id: Some(text_id),
                        quota_remaining: Some(quota_remaining),
                        error: None,
                    } => Ok(Success {
                        text_id,
                        quota_remaining,
                    }),

                    TextbeltResponse {
                        success: false,
                        error: Some(error),
                        quota_remaining: Some(0),
                        text_id: None,
                    } => Err(TextbeltError::OutOfQuota(error))?,

                    TextbeltResponse {
                        success: false,
                        error: Some(error),
                        quota_remaining: None,
                        text_id: None,
                    } => Err(TextbeltError::FailedWith(error))?,

                    TextbeltResponse {
                        error: opt_error, ..
                    } => Err(TextbeltError::InvalidResponse(opt_error))?,
                },
                Err(err) => bail!(err),
            },
            Err(err) => bail!(err),
        }
    }
}
