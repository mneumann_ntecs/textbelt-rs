use textbelt::{Client, PhoneNumber};

#[async_std::main]
async fn main() -> anyhow::Result<()> {
    let mut args = std::env::args();

    let key: String = args.next().expect("key");
    let phone: String = args.next().expect("phone");
    let message: String = args.next().expect("message");

    let client = Client::new(key);
    let response = client.send(&PhoneNumber(phone), &message).await?;
    println!("{:?}", response);
    Ok(())
}
